from django.test import TestCase

# Create your tests here.
from django.test import TestCase, Client
from django.urls import resolve
from . import models
from .views import *
from .models import *
from .forms import *
from .apps import TestimoniConfig
from django.apps import apps

class TestimonyUnitTest(TestCase):
    def test_url(self):
        response = Client().get('/testimoni/')
        self.assertEqual(response.status_code, 200)

    def test_url(self):
        response = Client().get('/testimoni/ulasan/')
        self.assertEqual(response.status_code, 200)

    def test_template(self):
        response = Client().get('/testimoni/')
        self.assertTemplateUsed(response, 'testimoni1.html')

    def test_template(self):
        response = Client().get('/testimoni/ulasan/')
        self.assertTemplateUsed(response, 'testimoni.html')

    def test_model_payment(self):
        Testimoni.objects.create(nama = "10" , pekerjaan = "dokter" , ulasan = "robbynurfadillah@gmail.com")
        counter = Testimoni.objects.all().count()
        self.assertEqual(counter, 1)

    def test_using_func(self):
        found = resolve('/testimoni/')
        self.assertEqual(found.func, display_testimoni)

    def test_get(self):
        response = Client().get('/testimoni/')
        html_kembalian = response.content.decode('utf8')

    def test_content_testimoni(self):
        response = Client().get('/testimoni/')
        html_return = response.content.decode('utf8')
        self.assertIn("Testimoni", html_return)
        self.assertIn("Ini adalah apa kata mereka tentang Crowdvid", html_return)
        self.assertIn("Ingin berbagi juga?", html_return)

    def test_konten_form_testimoni(self):
        response = Client().get('/testimoni/ulasan/')
        isi = response.content.decode('utf8')
        self.assertIn("Testimoni", isi)
        self.assertIn("Nama", isi)
        self.assertIn("Pekerjaan", isi)
        self.assertIn("Ulasan", isi)
