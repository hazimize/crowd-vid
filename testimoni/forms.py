from django import forms

class formulir_testimoni(forms.Form):
    nama = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Nama',
        'type' : 'text',
        'required' : True
    }))
    pekerjaan = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Pekerjaan',
        'type' : 'text',
        'required' : True
    }))
    ulasan = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Ulasan',
        'type' : 'text',
        'required' : True
    }))
