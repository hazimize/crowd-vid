from django.urls import path
from . import views
from .views import *


app_name = "testimoni"

urlpatterns = [
    path('ulasan/', formulir_testimoni, name='testimoni'),
    path('', display_testimoni, name='d_testimoni'),
#     path('ulasan/',formulir_testimoni, name = 'testimoni'),
]
