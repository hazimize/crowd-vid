from django import forms
from .models import FormKritikSaran
class FormulirKritikSaran(forms.ModelForm):
    class Meta:
        model = FormKritikSaran
        fields ='__all__'
        labels = {
            'namainitial':('Nama Initial'),
            'kritik':('Kritik'),
            'saran': ('Saran'),
            
        }
    



