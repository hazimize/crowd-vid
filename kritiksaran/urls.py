from django.urls import path

from . import views


app_name = 'kritiksaran'

urlpatterns = [
    path('', views.display_kritik_saran, name='display_kritik_saran'),
    path('form-kritik-saran/', views.form_kritik_saran, name='form_kritik_saran'),
]
